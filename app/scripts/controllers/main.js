'use strict';

/**
 * @ngdoc function
 * @name bjornApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bjornApp
 */
angular.module('bjornApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

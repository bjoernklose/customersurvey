'use strict';

/**
 * @ngdoc overview
 * @name bjornApp
 * @description
 * # bjornApp
 *
 * Main module of the application.
 */
angular
  .module('bjornApp', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngTouch'
  ]);
